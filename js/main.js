document.addEventListener("DOMContentLoaded", function() {
    document.querySelector(".get_down").onclick = function() {
        var tar = document.querySelectorAll("section")[1];
        smoothScroll(tar, 200);
    };

    function getTop(element, start) {
        if(element.nodeName === 'HTML') return -start
        return element.getBoundingClientRect().top + start
    }
    function animation(t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 }

    function position(start, end, elapsed, duration) {
        if (elapsed > duration) return end;
        return start + (end - start) * animation(elapsed / duration);
    }

    function smoothScroll(el, duration){
        var start = window.pageYOffset,
            end = getTop(el, start),
            clock = Date.now();

        var requestAnimationFrame = window.requestAnimationFrame ||
            window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame ||
            function(fn){window.setTimeout(fn, 15);};

        var step = function(){
            var elapsed = Date.now() - clock;
            window.scroll(0, position(start, end, elapsed, duration));

            if (elapsed > duration) {
                if (typeof callback === 'function') {
                    callback(el);
                }
            } else {
                requestAnimationFrame(step);
            }
        }
        step();
    }




    var tar = document.querySelector("main"),
        body = document.querySelector("body"),
        nav = document.querySelector("nav");

    document.getElementById("hamburger").onclick = function() {
        if (tar.classList.contains('open')) {
            tar.classList.remove('open');
            body.classList.remove('fixed');
            nav.classList.remove('open');
        } else {
            tar.classList.add('open');
            body.classList.add('fixed');
            nav.classList.add('open');
        }
    }
    document.getElementById("close").onclick = function() {
        tar.classList.remove('open');
        body.classList.remove('fixed');
        nav.classList.remove('open');
    }

    if (Function('/*@cc_on return document.documentMode<=10@*/')()) {
        document.body.innerHTML = "<h1 style='color: #414141;'>To see the website of Startup Studio Nescio, please update to a modern browser.</h1>";
    };

    var quoteEl = document.getElementById("quote");
    if (quoteEl != null) {
        var quotes = [{
            "id": 1,
            "quote": "Stay hungry, stay foolish.",
            "author": "Steve Jobs"
        }, {
            "id": 2,
            "quote": "I pity the fool",
            "author": "Mr. T"
        }];
        quoteEl.onclick = function() {
            var quote = quotes[Math.floor(Math.random() * quotes.length)];
            this.innerHTML = "\"" + quote.quote + "\"<h4>" + quote.author + "</h4>";
        }
    }

    var images = document.querySelectorAll(".image"),
        popup = document.getElementById('popup'),
        pop_img = document.getElementById('img'),
        pop_name = document.getElementById("name"),
        pop_job = document.getElementById("job"),
        pop_bio = document.getElementById("bio"),
        pop_social = document.getElementById("social"),
        pop_close = document.getElementById("popup_close"),
        overlay = document.getElementById("overlay"),
        body = document.getElementsByTagName('body')[0];

    if (images != null) {
        for (var i = 0; i < images.length; i++) {
            images[i].onclick = function(x) {
                x.preventDefault();
                var img = this.querySelectorAll("img")[0];
                var img = img.src;

                var name = this.querySelectorAll("h4")[0];
                var name = name.innerHTML;

                var job = this.querySelectorAll(".job")[0];
                var job = job.innerHTML;

                var bio = this.querySelectorAll(".bio")[0];
                var bio = bio.innerHTML;

                var social = this.querySelectorAll(".social")[0];
                var social = social.innerHTML;

                pop_img.src = img;
                pop_name.innerHTML = name;
                pop_job.innerHTML = job;
                pop_bio.innerHTML = bio;
                pop_social.innerHTML = social;
                popup.classList.add('visible');
                overlay.classList.add('visible');
                body.classList.add('fixed');
            }
        }
    }

    if (pop_close != null) {
        pop_close.onclick = function() {
            popup.classList.remove('visible');
            overlay.classList.remove('visible');
            body.classList.remove('fixed');
        };
    };

    if (overlay != null) {
        overlay.onclick = function() {
            popup.classList.remove('visible');
            overlay.classList.remove('visible');
            body.classList.remove('fixed');
        };
    };



    function fullHeight() {
        var elems = document.getElementsByClassName('full_height'),
            height = window.innerHeight;
        for (var i = 0; i < elems.length; i++) {
            if(height >= 768) {
              elems[i].style.height = height + 'px';
            }
            elems[i].style.minHeight = height + 'px';
        }
    };

    fullHeight();

    function pickImage() {
        var rand = Math.floor((Math.random() * 3) + 1),
            el = document.getElementById('home'),
            par = "background-image: url('images/home/home_" + rand + "s.jpg')",
            img = new Image(),
            url = 'images/home/home_' + rand + '.jpg';

        if (el != null) {
            el.setAttribute('style', par);

            img.src = url;
            img.onload = function() {
                el.setAttribute('style', "background-image: url('" + url + "')");
            };
        }



    }

    pickImage();

});
